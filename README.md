CURSO: Marketing Digital
=============

Docente: Pablo Ezequiel Vega. <br>
Coordinadores: Dr. Cristian Martínez. Lic. Carlos Ismael Orozco

Acerca del curso: El mismo forma parte de una vinculación entre el Departamento de Informática ([DIUNSa](http://di.unsa.edu.ar)) y la Escuela de Arte y Oficio de la Provincia de Salta.

**Condiciones para la obtención de certificado:**
- Certificado de asistencia: un 70% de asistencia mínimo a las clases
- Constancia de aprobación: un 60% de asistencia mínimo a clases y aprobación de un trabajo integrador.

**Temario**

[[_TOC_]]

# Semana 1: Introducción
Clase 1: Presentación
- [Diapositiva](https://gitlab.com/ciorozco/2020-marketing-digitial/-/blob/master/Semana%201/EAO_DIUNSa_MD_clase1.pdf) <br>
- [Video](https://www.youtube.com/watch?v=7HCc_vXsMCQ&feature=youtu.be) <br>

Clase 2: Facebook
- [Diapositiva](https://gitlab.com/ciorozco/2020-marketing-digitial/-/blob/master/Semana%201/EAO_DIUNSa_clase2.pdf) <br>
- [Video Comisión 1](https://www.youtube.com/watch?v=4z7GrAT3t4Y) <br>
- [Video Comisión 2](https://www.youtube.com/watch?v=23W3g-DTZYM) <br>

# Semana 2
Clase 3: Mercado Pago - WhatsApp Bussiness
- [Video Comisión 1](https://youtu.be/MmvZrNzC8B8) <br>
- [Video Comisión 2](https://youtu.be/n4s9NGIGGuU) <br>

Clase 4: Instagram
- [Video Comisión 1 y 2](https://youtu.be/1qDzOorvHiw) <br>

# Semana 3
Clase 5: Herramientas de publicación: Canva
- Herramienta : https://www.canva.com/
- [Video Comisión 1](https://youtu.be/YdxwM3KPV20) <br>
- [Video Comisión 2](https://youtu.be/TAUjZ2NcYYQ) <br>

Clase 6: Google My Bussiness
- Pagina web: https://www.google.com/intl/es-419_ar/business/
- [Video Comisión 1](https://youtu.be/J803wFUKdSI) <br>
- [Video Comisión 2](https://youtu.be/GKjs4saRlPM) <br>

# Semana 4
Clase 7: Compra en tu barrio
- Pagina : https://compraentubarrio.gob.ar/
- [Video Comisión 1](https://youtu.be/_cnH5za9XsY) <br>
- [Video Comisión 2](https://youtu.be/RhQw5183LJ4) <br>

# Informe final
Presentacion del Informe:
- [Plantilla informe](https://docs.google.com/document/d/1QvhFjyOW2Atpn4ZrZ4l-sW09Jx0Dz3GUhQBmjuy7h1M/edit?usp=sharing)

